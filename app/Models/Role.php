<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class Role {   

    public function get() {
       	$results = DB::select('select * from master_role');
       	if ($results) {
	        return $results;
       	} else {
       		return false;
       	} 
    }

    public function getById($id) {
    	$results = DB::select('select * from master_role where role_id = '.$id);
    	if ($results) {
	        return $results;
       	} else {
       		return false;
       	} 
    }

    public function create($data) {
    	$results = DB::table('master_role')->insert($data);
    	if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }

    public function update($data) {
    	$results = DB::table('master_role')
            ->where('role_id', $data['id'])
            ->update($data);
        if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }

    public function delete($data) {
    	$results = DB::table('master_role')->where('role_id', $data['id'])->delete();
    	if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }
}
