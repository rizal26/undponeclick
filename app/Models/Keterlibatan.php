<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class Keterlibatan {   

    public function get() {
       	$results = DB::select('select * from master_keterlibatan');
       	if ($results) {
	        return $results;
       	} else {
       		return false;
       	} 
    }

    public function getById($id) {
    	$results = DB::select('select * from master_keterlibatan where keterlibatan_id = '.$id);
    	if ($results) {
	        return $results;
       	} else {
       		return false;
       	} 
    }

    public function create($data) {
    	$results = DB::table('master_keterlibatan')->insert($data);
    	if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }

    public function update($data) {
    	$results = DB::table('master_keterlibatan')
            ->where('keterlibatan_id', $data['id'])
            ->update($data);
        if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }

    public function delete($data) {
    	$results = DB::table('master_keterlibatan')->where('keterlibatan_id', $data['id'])->delete();
    	if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }
}
