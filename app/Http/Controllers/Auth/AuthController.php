<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use App\Helpers\Utils;

class AuthController extends Controller
{
    public function login(Request $request) {
        if(Session::has('login') && Session::get('login')) {
            return Redirect::to('/dashboard');
        }

        if($request->isMethod('post')) {
            $login_id = $request->input('username');
            $password = $request->input('password');
            if($login_id == 'admin' && $password == 'admin') {
                Session::put('nama', 'Admin');
                Session::put('login', TRUE);
                Session::forget('userdata');
                return redirect('/home');
            } else {
                return redirect()->back()->withErrors(['error' => 'Authentication Failed.']);
            }
        }
        return view('auth.login');
    }

    public function logout(Request $request) {
        Session::forget('login');
        return redirect('/login');
    }
}