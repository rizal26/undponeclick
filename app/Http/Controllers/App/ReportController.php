<?php
namespace App\Http\Controllers\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class ReportController extends Controller {

    public function __construct() {
        // $this->middleware('auth');
    }

    public function index(Request $request) {
        return view('web.apps.report.index');
    }

}