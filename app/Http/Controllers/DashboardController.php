<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class DashboardController extends Controller {

    public function __construct() {
        // $this->middleware('auth');
    }

    public function index(Request $request) {
        return view('web.dashboard');
    }

}