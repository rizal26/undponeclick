<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(!(Session::get('login'))){
        return redirect('login');
    }
    return redirect('home');
});

//login
Route::match(['get','post'], '/login', 'Auth\AuthController@login')->name('login');
Route::get('/login',['as'=>'login','uses'=>'Auth\AuthController@login']);
Route::get('/logout', 'Auth\AuthController@logout')->name('logout');

Route::get('/home', 'DashboardController@index')->name('home');
Route::get('/report', 'App\ReportController@index')->name('report');
