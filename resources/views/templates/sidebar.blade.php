            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            

                        	<li class="text-muted menu-title">Navigation </li>

                            <li>
                                <a href="{{ url('/home') }}" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                            </li>
                            <li>
                                <a href="{{ url('/report') }}" class="waves-effect"><i class="ti-bar-chart-alt"></i> <span> Report </span></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->