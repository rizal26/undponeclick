@extends('templates.auth')
@section('content')

    <div></div>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class="card-box card-box-auth" style="opacity: 0.95;">
            <div class="panel-heading text-center" style="padding-bottom: 0px;"> 
                <h3 class="text-center"> 
                    Login UNDP
                </h3>
            </div> 
            <div class="panel-body">
            @if ($errors->has('error'))
                <div class="alert alert-danger">
                    <strong>{{ $errors->first('error') }}</strong>
                </div>
            @endif
            <form class="form-horizontal m-t-20" action="" method="post">
                {{csrf_field()}}
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input name="username" class="form-control" type="text" required="" placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input name="password" class="form-control" type="password" required="" placeholder="Password">
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

            </form> 
            
            </div>
            
        </div>
@endsection